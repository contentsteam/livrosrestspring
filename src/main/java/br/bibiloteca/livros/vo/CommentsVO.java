package br.bibiloteca.livros.vo;

public class CommentsVO {

    private String comment;

    private Long evaluation;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Long evaluation) {
        this.evaluation = evaluation;
    }

    @Override
    public String toString() {
        return "CommentsVO{" +
                "comment='" + comment + '\'' +
                ", evaluation=" + evaluation +
                '}';
    }
}
