package br.bibiloteca.livros.vo;

import java.util.*;

public class BookVO {

    private Long id;

    private String titulo;

    private String autor;

    private String url;

    private Double avaliacao;

    private List<CommentsVO> comments = new ArrayList<>();

    public BookVO() { }

    public BookVO(Long id, String titulo, String autor, String url) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(Double avaliacao) {
        this.avaliacao = avaliacao;
    }

    public List<CommentsVO> getComments() {
        return comments;
    }

    public void setComments(List<CommentsVO> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "BookVO{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", url='" + url + '\'' +
                ", avaliacao=" + avaliacao +
                ", comments=" + comments +
                '}';
    }
}
