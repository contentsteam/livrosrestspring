package br.bibiloteca.livros.service;

import br.bibiloteca.livros.converter.BookConverter;
import br.bibiloteca.livros.repository.BookRepository;
import br.bibiloteca.livros.vo.BookVO;
import br.bibiloteca.livros.vo.CommentsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<BookVO> getBooks(){
        return bookRepository.getBooks().stream().map(x ->
                {
                    x.setAvaliacao(bookRepository.getAverageEvaluation(x.getId()));
                    return x;
                }
        ).collect(Collectors.toList());
    }

    public BookVO setComments(Long id, CommentsVO commentsVO) {
        BookVO bookVO = bookRepository.addComment(id, commentsVO);
        return bookVO;
    }

    public BookVO getBook(Long id) {
        return bookRepository.getBook(id);
    }
}