package br.bibiloteca.livros.repository;


import br.bibiloteca.livros.vo.BookVO;
import br.bibiloteca.livros.vo.CommentsVO;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

@Component
public class BookRepository {

    public static Map<Long, BookVO> books = new HashMap();

    public BookRepository() {
        books.put(1l, new BookVO(1l, "Memórias Postumas de Bráz Cubas", "Machado de Assis", "1.jpg" ));
        books.put(2l, new BookVO(2l, "Morte e Vida Severina", "João Cabral e Melo Neto", "2.jpg" ));
        books.put(3l, new BookVO(3l, "Auto da Compadecida", "Ariano Suassuna", "3.jpg" ));
        books.put(4l, new BookVO(4l, "O Cortiço", "Aluisio de Azevedo", "4.jpg" ));
    }

    public List<BookVO> getBooks(){
        return books.values().stream().map(x ->
                {
                    x.setAvaliacao(getAverageEvaluation(x.getId()));
                    return x;
                }
        ).collect(Collectors.toList());
    }

    public Double getAverageEvaluation(Long id){
        Double average = 0d;
        if(books.containsKey(id)){
            OptionalDouble averange = books.get(id).getComments()
                    .stream()
                    .mapToDouble(a -> a.getEvaluation())
                    .average();
            average =   averange.isPresent() ?  averange.getAsDouble() : 0;
        }
        return  average;
    }

    public BookVO getBook(Long id){
        BookVO book = books.get(id);
        book.setAvaliacao(getAverageEvaluation(id));
        return books.get(id);
    }

    public BookVO addComment(Long id, CommentsVO commentsVO) {
        BookVO book = books.get(id);
        book.getComments().add(commentsVO);
        book.setAvaliacao(getAverageEvaluation(id));
        return book;
    }
}
