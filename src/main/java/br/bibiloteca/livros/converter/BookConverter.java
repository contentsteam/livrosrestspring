package br.bibiloteca.livros.converter;

import br.bibiloteca.livros.vo.BookVO;

public class BookConverter {

    public static BookVO toBookVo(BookVO bookVO, Double average){
        bookVO.setAvaliacao(average);
        return bookVO;
    }

}
