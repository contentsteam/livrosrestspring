package br.bibiloteca.livros.controllers;

import br.bibiloteca.livros.service.BookService;
import br.bibiloteca.livros.vo.BookVO;
import br.bibiloteca.livros.vo.CommentsVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class BookController {

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @GetMapping("/book/list")
    public ResponseEntity<List<BookVO>> getBooks() {
        List<BookVO> books =  bookService.getBooks();
        logger.info("Get list Books");
        return new ResponseEntity<List<BookVO>>(books, HttpStatus.OK);
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<BookVO> getBooks(@PathVariable("id") Long id) {
        BookVO book =  bookService.getBook(id);

        logger.info("Insert comments Book " + book.toString() );

        return new ResponseEntity<BookVO>(book, HttpStatus.OK);
    }

    @PutMapping("/book/{id}/comments")
    public ResponseEntity<BookVO> setComments(@PathVariable("id") Long id, @RequestBody CommentsVO commentsVO) {
        BookVO book =  bookService.setComments(id, commentsVO);

        logger.info("Get Book " + book.toString() );

        return new ResponseEntity<BookVO>(book, HttpStatus.OK);
    }
}